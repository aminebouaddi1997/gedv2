package com.ged.repositories;

import com.ged.model.Image;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.Optional;

public interface ImageRepository extends MongoRepository<Image, Long> {
	Optional<Image> findByName(String name);
}
