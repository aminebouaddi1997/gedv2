package com.ged.services;


import com.ged.model.Image;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name ="DOCUMENT-SERVICE")
public interface DocumentRestClient {
    @GetMapping(path="")
    Image findDocumentById(@PathVariable String id);
}

