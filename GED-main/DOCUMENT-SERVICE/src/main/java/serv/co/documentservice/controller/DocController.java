package serv.co.documentservice.controller;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import serv.co.documentservice.model.Doc;

import serv.co.documentservice.model.Image;
import serv.co.documentservice.repository.DocRepository;
import serv.co.documentservice.util.ImageUtility;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/docudb")
public class DocController {

    private final DocRepository repository;
    private final MongoTemplate mongoTemplate;

    public DocController(DocRepository repository, MongoTemplate mongoTemplate) {
        this.repository = repository;
        this.mongoTemplate = mongoTemplate;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file,
                                             @RequestParam("name") String name,
                                             @RequestParam("description") String description,
                                             @RequestParam("image") MultipartFile imageFile) {
        try {
            // Save the document file
            byte[] fileData = file.getBytes();
            String checksum = DigestUtils.sha256Hex(fileData);

            // Save the image file
            byte[] imageData = imageFile.getBytes();
            String imageName = imageFile.getOriginalFilename();

            Doc doc = Doc.builder()
                    .name(name)
                    .docx(new String(fileData))
                    .checksum(checksum)
                    .description(description)
                    .image(Image.builder()
                            .name(imageName)
                            .type(imageFile.getContentType())
                            .image(ImageUtility.compressImage(imageData))
                            .build())
                    .build();

            repository.save(doc);

            return ResponseEntity.status(HttpStatus.OK).body("File uploaded successfully");
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("File upload failed");
        }
    }


    @GetMapping("/print")
    public void printAllDocs() {
        repository.findAll().forEach(System.out::println);
    }

    @GetMapping("/{id}")
    public Optional<Doc> getDocId(@PathVariable String id) {
        return repository.findById(id);
    }

    @GetMapping("/all")
    public List<Doc> getAllDocuments() {
        return repository.findAll();
    }

}
